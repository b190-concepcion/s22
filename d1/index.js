console.log("hello world");

/*
	try to store the following grades inside the variable and log the variable in the console.

	98.5
	94.3
	89.2
	90.1

*/

/*let grade1 = 98.5;
let grade2 = 94.3;
let grade3 = 89.2;
let grade4 = 90.1;

console.log(grade1);
console.log(grade2);
console.log(grade3);
console.log(grade4);*/

//we could actually do it like this using arrays
/*
	Arrays are used to store multiple related values in a single variable
	They are declared using square brackets[] also known "Array Literals
	Arrays also provide access to a number of functions/methods that help in achieving this tasks that we can perform  inside on the elements inside the arrays.
*/


/*
		Syntax:
		let/const arrayName = [elementA, elementB, elementC, ...., elementN];
*/
let grades = [98.5,94.3,89.2,90.1];
console.log(grades);


let computerBrands = ["Lenovo","Dell","Asus","HP","MSI","Acer","CDR-King"];

console.log(computerBrands);

const mixedArr = [12,"Asus",null,undefined];
// console.log(mixedArr);

//Reassigning of Array values

console.log("Array before reassigning: ");
console.log(mixedArr);

//array[index-position]
mixedArr[0] = "Hello World";

console.log("Array after reassigning: ");
console.log(mixedArr);


/*Section - Reading from Arrays
	Syntax:
	ArrayName[index]
	*/
console.log(grades);

console.log(grades[0]);
console.log(computerBrands[6]);
console.log(computerBrands.length);


if(computerBrands.length >5){
	console.log("We have too many suppliers. Pleae coordinate with the operations manager.")
}


//will get the last element of the array arrayName.length -1
let lastElementIndex = computerBrands.length -1;

console.log(computerBrands[lastElementIndex]);

//Section - ARRAY METHODS
// methods are similar to functions, theses array methods can be done in array and objects alone



//Mutator Methods


let fruits = ["Apple","Mango","Rambutan","Lanzones","Durian"];

console.log(fruits);

//push() - adds an element at the end of an array
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated Array from push method: ");
console.log(fruits);

/*fruits[6] = "Orange";
console.log(fruits);*/
fruits.push("Orange");

fruits.push("Guava", "Avocado");
console.log("Mutated Array from push method: ");
console.log(fruits);


//pop() - remove 1 at the end
let removefruit = fruits.pop();
console.log(removefruit);
console.log("Mutated Array from push method: ");
console.log(fruits);

//unshift - add elements at the start of an array


fruits.unshift("Lime", "Banana");
console.log("Mutated Array from unshift method: ");
console.log(fruits);

//shift - remove at the start

let fruitRemoved = fruits.shift();
console.log(fruits);

//splice - simultaneously removes and adds elements from specified index number.
/*
		Syntax:
		arrayname.splice(startingindex, delete count, elementsToBeAdded)
*/


fruits.splice(1,2, "Lime","Cherry");
console.log("Mutated Array from Splice method: ");
console.log(fruits);


//sort - rearranges the elements in an array in alphanumeric order
/*
	Syntax:
		arrayName.sort();
*/
fruits.sort();
console.log("Mutated Array from Sort method: ");
console.log(fruits);

//reverse - rearranges the elements in an array in descending order
/*
	Syntax:
		arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated Array from Reverse method: ");
console.log(fruits);

//Non-Mutator Methods
/*
	-are functions that do not modify the original array
	-these methods do not manipulate the elents inside the array even they are performing task such as returning elements from an array and combining them with other arrays and printing the output
*/

//indexOf() 

let countries = ["RUS","CH","JPN","PH","USA","KOR","AUS","CAN","PH"];


let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf: " + firstIndex);

firstIndex = countries.indexOf("KAZ");
console.log("Result of indexOf: " + firstIndex);

//lastIndexOf() - starts the search process from the last down to first


let lastIndex = countries.lastIndexOf("PH");
console.log("Result of indexOf: " + lastIndex);


//slice
/*
	copies a part of the array and stores it inside a new array
	Syntax:
	arrayName.slice(startingindex);
	arrayName.slice(startingindex,endingelement);
*/
let sliceArrayA = countries.slice(2);
console.log("Result of slice: " + sliceArrayA);
console.log(countries);

let sliceArrayB = countries.slice(4,7);
console.log("Result of slice: " + sliceArrayB);
console.log(countries);


//slicing off the elements from the end of the array
let sliceArrayC = countries.slice(-3);
console.log("Result of slice: " + sliceArrayC);
console.log(countries);

//toString() - returns a new array as a string separated by commas

let stringArray = countries.toString()
console.log("Result of toString: ");
console.log(stringArray);


//concat() - used to combine two arrays and returns the combined result

let taskA = ["drink HTML","eat Javascript"];
let taskB = ["inhale CSS","breath SASS"];
let taskC = ["get GIT","be node"];


let tasks = taskA.concat(taskB);
console.log("Result of concat ");
console.log(tasks);

//combining multiple arrays

let allTasks = taskA.concat(taskB,taskC);
console.log("Result of multi concat ");
console.log(allTasks);

//join() - returns an arrays as string separated by specified string separator
/*
	
*/
let users = ["John","Jane","Joe","Jobert","Julius"];
console.log(users);
console.log(users.join());
console.log(users.join(''));
console.log(users.join('-'));

//Iterator methods
/*
	-iteration methods are loops designed to perform repetitive task on arrays
	-useful for manipulating array data resulting in complex tasks
*/

/*forEach - similar to for loop that loops through all elements
 			-variable names for arrays are usually written in plural form of the data stored in an array
 			Syntax:
 			arrayName.forEach(fucntion(individualElement){
					statement/s
 			})

 */
allTasks.forEach(function(task){
	console.log(task);
})

//forEach with conditional statements

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length >10){
		//we stored the filtered elements inside another variable to avoid confusion should we need the original array in tact
		filteredTasks.push(task);
	}
})
console.log("Result of forEach ");
console.log(filteredTasks);

//map
/*
	-iterates on each element AND returns a new array with different values depending on the result of tyhe function's operations.
	Syntax:
	let/const resultArray = arrayName.map(function(individualelement){
		return statement
	})
*/

let numbers = [1,2,3,4,5];

let numbersMap = numbers.map(function(number){
	return number * number;
})
console.log("Result of map: ");
console.log(numbersMap)

//every() - returns a boolean data type depending if all elements meet the condition or not
let allValid = numbers.every(function(number){
	//not all numbers are less than 3 = false
	return(number<3);
})
console.log("Result of every: ");
console.log(allValid);
console.log(numbers);


//some()

let someValid = numbers.some(function(number){
	return (number<2);
})
console.log("Result of some: ");
console.log(someValid);
console.log(numbers);

//filter

let filterValid = numbers.filter(function(number){
	return (number<3);
})
console.log("Result of filter: ");
console.log(filterValid);
console.log(numbers);


let parts =["Mouse", "Keyboard", "Unit", "Monitor"];
console.log(parts);

//reduce
/*
	evaluates elements from left to right and returns/reduces the array into a single value
*/


let iteration = 0;

//using array of numbers
let reducedArray = numbers.reduce(function(x,y){
	console.warn(iteration);
	console.log(x);
	console.log(y);
	return x+y;
})
console.log(reducedArray);

//using array of strings
let reducedStringArray = parts.reduce(function(x,y){
	console.warn(iteration);
	console.log(x);
	console.log(y);
	return x+y;
})
console.log(reducedStringArray);


//includes method - searches each of the elements if they have the specific character that is inside it''s argument
let filteredParts = parts.filter(function(part){
	return part.toLowerCase().includes("n");
});
console.log(filteredParts);